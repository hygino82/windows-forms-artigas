namespace FormControle
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Label label1;
        Button btn1;

        private void Form1_Load(object sender, EventArgs e)
        {
            label1 = new Label();
            label1.Text = "Minha label no c�digo";
            label1.Location = new Point(10, 10);
            label1.AutoSize = false;
            label1.Size = new Size(400, 100);
            label1.BackColor = Color.White;
            label1.ForeColor = Color.Black;
            label1.Font = new Font(FontFamily.GenericSansSerif, 28, FontStyle.Bold, GraphicsUnit.Point);

            btn1 = new Button();
            btn1.Location = new Point(250, 285);
            btn1.Size = new Size(200,60);
            btn1.Text = "Bot�o c�digo";
            btn1.Font = new Font("Arial",10,FontStyle.Bold,GraphicsUnit.Point);

            btn1.Click += btn1_click;
            btn1.MouseEnter += btn1_MouseEnter;
            this.Controls.Add(label1);
            this.Controls.Add(btn1);

        }

         private void btn1_click(object sender,EventArgs e)
         {
            label1.Text = "Testando";
         }

         private void btn1_MouseEnter(object sender,EventArgs e)
         {
            label1.Text = "Passou o mouse";
         }
        
    }
}