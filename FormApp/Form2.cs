﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormApp
{
    public partial class Form2 : Form
    {
        public string Mensagem { get; set; }

        public Form2()
        {
            InitializeComponent();
        }

        public Form2(string mensagem)
        {
            InitializeComponent();
            this.Mensagem = mensagem;
        }

        private void btnPrincipal_Click(object sender, EventArgs e)
        {
            this.Close();
            Thread t = new Thread(() => Application.Run(new Form1()));
            t.Start();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            txtMensagem.Text = Mensagem;
        }

        private void btnRetorno_Click(object sender, EventArgs e)
        {
            if (txtMensagem.Text == "" || txtMensagem.Text == null)
            {
                Mensagem = null;
            }
            else
            {
                Mensagem = txtMensagem.Text;
            }
            this.Close();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Mensagem = null;
            this.Close();
        }
    }
}
