﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnSegundaThread_Click(object sender, EventArgs e)
        {
            this.Close();
            Thread t = new Thread(() =>
            {
                Application.Run(new Form2());
            });
            t.Start();
        }

        private void btnSegunda_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 f = new Form2("Dilma Opressora");
            //f.Mensagem = "Dilma Opressora";
            f.ShowDialog();
            if (f.Mensagem != null)
            {
                lblTitulo.Text = f.Mensagem;
            }
            this.Show();
        }

        private void menuNovo_Click(object sender, EventArgs e)
        {
            new Thread(() => Application.Run(new Form1())).Start();
        }

        private void menuAbrir_Click(object sender, EventArgs e)
        {
            Hide();
            Form2 f = new Form2();
            f.ShowDialog();
            Show();
        }

        private void menuSair_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void menuDev_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Adroaldo Ferreira hygino82@gmail.com");
        }

        private void menuVersao_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Versão 1.0");
        }

        private void comboMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboMenu.SelectedIndex == 0)
            {
                menuFile.Text = "File";
                menuAjuda.Text = "Help";
            }
            else
            {
                menuFile.Text = "Arquivo";
                menuAjuda.Text = "Ajuda";
            }
        }

        private void textMenu_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                lblTitulo.Text = textMenu.Text;
                textMenu.Text = "";
            }
        }
    }
}
